package fr.dev.firefighter.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dev.firefighter.entity.Firefighter;
import fr.dev.firefighter.services.FirefighterService;

@RestController
@RequestMapping(path = "/app")
public class FirefighterRestController {

	@Autowired
	private FirefighterService fireService;
	
	@PostMapping(path = "/firefighter/new", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> listFirefighterWeek(@RequestBody Firefighter firefighter){
				
		ArrayList<Firefighter> listFirefighter = fireService.firefighterOfDay(firefighter);
		
	    if(listFirefighter != null) {
	    	return new ResponseEntity<ArrayList<Firefighter>>(listFirefighter, HttpStatus.OK);
	    }else {
	    	 return ResponseEntity
			            .status(HttpStatus.NOT_FOUND)
			            .body("404 : Ya pas la list des pompiers du jour");
	    }
		
	}
	
}