package fr.dev.firefighter.entity;

public class Firefighter {

	int id;
	String nom;
	String team;
	
	public Firefighter(int id, String nom, String team) {
		super();
		this.id = id;
		this.nom = nom;
		this.team = team;
	}
		
	public Firefighter() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

}
