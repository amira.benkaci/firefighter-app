package fr.dev.firefighter.services;

import java.util.ArrayList;

import fr.dev.firefighter.entity.Firefighter;

public interface InterfaceFirefighterService {
	
	public ArrayList<Firefighter> firefighterOfDay(Firefighter fighter);
}
