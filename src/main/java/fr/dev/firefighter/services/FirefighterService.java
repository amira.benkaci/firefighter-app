package fr.dev.firefighter.services;

import java.util.ArrayList;
import java.util.Collections;

import org.springframework.stereotype.Service;

import fr.dev.firefighter.entity.Firefighter;
import fr.dev.firefignter.NomSort;

@Service
public class FirefighterService implements InterfaceFirefighterService {

	public ArrayList<Firefighter> firefighterOfDay(Firefighter fighter) {
			
		ArrayList<Firefighter> fireData = data();
		fireData.sort(new NomSort());
		int indexDesignedFire = fireData.indexOf(fighter);			
		for (Firefighter firefighter : fireData) {
			if(firefighter.getNom().equals(fighter.getNom())) {
				indexDesignedFire = fireData.indexOf(firefighter);
			}
		}	
		Collections.reverse(fireData);
		Collections.rotate(fireData,indexDesignedFire);
		Collections.reverse(fireData);			
		
		return fireData;
	}
	
	
	public ArrayList<Firefighter> data(){
		Firefighter fire4 = new Firefighter(1, "Hugo", "Core Qualité");
		Firefighter fire3 = new Firefighter(3, "Renaud", "Core Qualité");
		Firefighter fire5 = new Firefighter(5, "Romain", "Core Qualité");
		Firefighter fire2 = new Firefighter(2, "fanch", "Core Qualité");
		Firefighter fire1 = new Firefighter(4, "Amira", "Android");
		
		ArrayList<Firefighter> listData = new ArrayList<Firefighter>();
		
		listData.add(fire1);	
		listData.add(fire2);	
		listData.add(fire3);		
		listData.add(fire4);	
		listData.add(fire5);
		
		return listData;
	}
	

}
