package fr.dev.firefignter;

import java.util.Comparator;

import fr.dev.firefighter.entity.Firefighter;

public class NomSort implements Comparator<Firefighter>{

	public int compare(Firefighter o1, Firefighter o2) {
		// TODO Auto-generated method stub
		return o1.getNom().compareToIgnoreCase(o2.getNom());
	}

}
