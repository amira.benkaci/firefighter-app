package fr.dev.firefignter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import fr.dev.firefighter.controllers.FirefighterRestController;
import fr.dev.firefighter.services.FirefighterService;



@SpringBootApplication(scanBasePackages= {"fr.dev.firefignter.controllers", "fr.dev.firefignter.entity", 
		"fr.dev.firefignter.services"})
@ComponentScan(basePackageClasses = FirefighterRestController.class)
@ComponentScan(basePackageClasses = FirefighterService.class)
public class FirefighterAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirefighterAppApplication.class, args);
		System.out.println("Start OK !");
	}


}
