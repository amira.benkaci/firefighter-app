package fr.dev.firefignter.servicesTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.dev.firefighter.entity.Firefighter;
import fr.dev.firefighter.services.FirefighterService;

@SpringBootTest
public class FirefighterServiceTest {

	@Autowired
	FirefighterService firefighterService;
	
	@DisplayName("Test Spring @Autowired Integration")
	@Test
	void firefighterOfDayTest(){
		Firefighter fire = new Firefighter(1, "Hugo", "Core Qualité");
		ArrayList<Firefighter> listFire = firefighterService.firefighterOfDay(fire);
		String[] dataTab = {"Hugo", "Renaud", "Romain","Amira","fanch"};
		int lenData = dataTab.length;
		int lenFireList = listFire.size();
		for (int i = 0; i < lenData && i < lenFireList; i++ ) {
			String nomData = dataTab[i];
			String nomFire = listFire.get(i).getNom();
			assertThat(nomData).isEqualTo(nomFire);
		}		
	}
}
