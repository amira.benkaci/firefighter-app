package fr.dev.firefignter;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import fr.dev.firefignter.servicesTest.FirefighterServiceTest;

@SpringBootTest
@ComponentScan(basePackageClasses = FirefighterServiceTest.class)
class FirefighterAppApplicationTests {

	@Test
	void contextLoads() {
	}

}
