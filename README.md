# Firefighter app


## Mise en place
### Pour lancer le JAR, utiliser la commande ci-dessous:
java -jar firefighterApp-0.0.1-SNAPSHOT.jar

### Pour tester L'api:
    1/ Lancer le Jar
    2/ utilisait un outil de test Api comme postman
    3/ Choisir la méthode POST
    4/ Inserer l'url : localhost:8080/app/firefighter/new
    5/ Cliquer sur Body puis raw, et choisir JSON(application/json)
    6/ Mettre dans le body par exemple et cliquer sur send:
        {
            "id": 1,
            "nom": "Hugo",
            "team": "Core Qualité"
        }
    5/ On aura la réponse de la requête dans le body en dessous, et choisir le format JSON pour formater la réponse en JSON.    
